const QRCode = require('./../models/qrModel');

exports.getCount = async (req,res,next) => {
    try {
        const count = await QRCode.find();
        res.status(200).json({data:count,status:'success'});
    } catch (error) {
        return res.status(500).json({err:error.message});
    }
}

exports.createCount = async (req,res) => {
    try {
        const count = await QRCode.create(req.body);
        res.json({data:count,status:'success'});
    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

exports.getCoin = async (req,res) => {
    try {
        const count = await QRCode.findById(req.params.id);
        if (!count) {
            return res.status(404).json({ status: 'error', message: 'QRCode not found' });
        }
        count.count -= 1;
        
        await count.save();

        res.json({ data: count, status: 'success' });

    } catch (error) {
        res.status(500).json({err:error.message});
    }
}