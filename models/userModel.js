const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true,'Please provide your name!']
    },
    CID: {
        type:Number,
        required:[true,'Please provide your citizen ID!'],
        maxlength:11
    },
    balance: {
        type:Number,
        default:0
    },
    label: {
        type: String,
        enum: ['passenger','driver'],
        default:'passenger'
    },
    gender: {
        type: String,
        required:[true,'Please provide your gender!']
    },
    age: {
        type: Number,
        required:[true,'Please provide your age!']
    },
    phoneNumber: {
        type: Number,
        require:[true,'Please provide your phone number!'],
        maxlength:8
    },
    password: {
        type: String,
        required:[true,'Please provide a password!'],
        minlength:8,
        select: false
    },
    passwordConfirm: {
        type: String,
        required:[true,'Please confirm your password']
    }
})

const User = mongoose.model('User',userSchema);
module.exports = User;