const mongoose = require('mongoose');

const qrSchema = new mongoose.Schema({
    count: {
        type: Number
    }
})

const QRCode = mongoose.model('QRCode',qrSchema);
module.exports = QRCode;