const express = require('express');
const app = express();
const QrCodeRouter = require('./routes/QrCodeRoute');
const cors = require('cors');

app.use(cors());

app.use(express.json());
app.use('/api/v1/qr',QrCodeRouter);

module.exports = app;

