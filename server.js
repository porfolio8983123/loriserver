const app = require('./app')
const dotenv = require('dotenv');
dotenv.config()
const mongoose = require('mongoose');

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD
)

mongoose.connect(DB)
.then((con) => {
    console.log("DB successfully connected!");
})
.catch(error => {
    console.log(error.message);
})

app.listen(process.env.PORT,() => {
    console.log("Server connected to ", process.env.PORT);
})