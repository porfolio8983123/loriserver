const express = require('express');
const router = express.Router();
const qrController = require('./../controllers/qrController');

router 
    .route('/')
    .get(qrController.getCount)
    .post(qrController.createCount)

router 
    .route('/:id')
    .get(qrController.getCoin)

module.exports = router;